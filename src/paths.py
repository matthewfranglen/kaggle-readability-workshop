""" Common paths used by the local code.
    For kaggle kernels define the paths in the kernel.
    Remember that the kernel paths depend on the loaded dependencies. """

from pathlib import Path

PROJECT_ROOT = Path(__file__).resolve().parents[1]
DATA_FOLDER = PROJECT_ROOT / "data"
DATA_RAW_FOLDER = DATA_FOLDER / "raw"

DATA_CSV_TRAIN = DATA_RAW_FOLDER / "train.csv"
DATA_CSV_TEST = DATA_RAW_FOLDER / "test.csv"
DATA_CSV_SAMPLE_SUBMISSION = DATA_RAW_FOLDER / "sample_submission.csv"
